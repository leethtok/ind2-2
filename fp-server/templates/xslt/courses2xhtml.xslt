<?xml version="1.0"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://www.w3.org/1999/xhtml"
        xmlns:courses="http://www.w3.org/2005/courses"
        xmlns="http://www.w3.org/1999/xhtml"
        version="1.0">

    <xsl:import href="html5.xslt"/>
 
    <xsl:key name="entry-by-category" match="courses:course" use="courses:course[1]/@term" />

    <xsl:template match="/courses:rating">
       <html>
                <head>
                    <title><xsl:value-of select="courses:course" /></title>
                    <link rel="alternate" type="application/courses+xml" href="{courses:link[@rel='self']/@href}" />
                    <link rel="Stylesheet" href="style.css"/>
                </head>
                <body id="report">
                    <xsl:apply-templates select="courses:entry[generate-id(.) = generate-id(key('entry-by-category', courses:status/@term)[1])]">
                        <!-- courses:entry[count(. | key('entry-by-category', courses:status/@term)[1]) = 1] -->
                        <xsl:sort select="courses:category/@term" />
                    </xsl:apply-templates>
                </body>
            </html>
    </xsl:template>

    <xsl:template match="courses:course">
        <h1><xsl:value-of select="courses:complete/@term" /></h1>
        <ul>
            <xsl:for-each select="key('entry-by-category', courses:course/@term)">
                <xsl:sort select="courses:complete" order="descending"/>
                <li>
                        <span><xsl:value-of select="courses:course" /></span>
                        <xsl:value-of select="concat(' ',courses:rating)" />

                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>