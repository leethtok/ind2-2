const express = require('express');
const router = express.Router();

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var schema = {
  "type": "array",
  "minItems": 10,
  "maxItems": 20,
  "items": {
	  "type": "object",
	  "properties": {
      "course":{
        "type": "string",
        "faker":"lorem.word"
      },
      "complete":{
        "type": "integer", 
	      "minimum": 0,
  		  "maximum": 100
      },
	    "total" : {
	      "type": "integer", 
	       "minimum": 0,
  		   "maximum": 100
      },
      "rating" : {
	      "type": "integer", 
	       "minimum": 0,
  		   "maximum": 5
	    },
	   
      },
	  "required": [
	    "course", 
	    "complete",
      "total",
      "rating",
	   ]
   }
	  };

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   console.log(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('course', 
	  	{  course:  sample });
  });

  
});

module.exports = router;
